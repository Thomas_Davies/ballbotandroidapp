package autodesk.com.ballbotandroidapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;


/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for
 * incoming connections, a thread for connecting with a device, and a
 * thread for performing data transmissions when connected.
 */
public class BTClient {
    private static final String TAG = "BTClient";
    private static final UUID MY_UUID = UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee");

    //Members!
    private final BluetoothAdapter mBluetoothAdapter;
    private final Handler mHandler;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private AcceptThread mAcceptThread;

    private int mState;

    //Connection State Constants
    public static final int STATE_NONE = 0;
    public static final int STATE_LISTENING = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_CONNECTED = 3;

    // Name for the SDP record when creating server socket
    private static final String CLIENT_NAME = "BallBotClient";


    /**
     * Constructor, prepares new bluetooth session
     *
     * -Context: The main UI activity context
     * -handler: a handler to send msg's back to main UI activity
     */
    public BTClient(Context context, Handler handler) {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mHandler = handler;
    }

    /**
     * Set the current state of client
     */
    private synchronized void setState( int state){
        mState = state;

        //pass new state to handler for UI update
        mHandler.obtainMessage(MainActivity.MESSAGE_STATE_CHANGED,state,-1).sendToTarget();
    }

    /**
     * Return the current connection state.
     */
    public synchronized int getState() {
        return mState;
    }

    /**
     * Start the BT communication client!
     */
    public synchronized void start() {
        Log.d (TAG,"start");
        //Cancel any thread currently attempting to make a connection
        if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}

        //cancel any thread currently running a connection
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

        setState(STATE_LISTENING);

        //start the thread to listen on a BluetoothServerSocket
        if (mAcceptThread == null) {
            mAcceptThread = new AcceptThread();
            mAcceptThread.start();
        }
    }

    /**
     * Start the connect thread to initiate bluetooth connection
     * @param device: the BluetoothDevice to connect to
     */
    public synchronized void connect(BluetoothDevice device){
        Log.d("BTConnectDebug","connect to: " + device);

        //cancel any thread attempting to make connection
        if (mState == STATE_CONNECTING){
            if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
        }

        //cancel any thread currently connected
        if (mConnectedThread != null) {mConnectedThread.cancel(); mConnectedThread = null;}

        //start thread to connect to device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    /**
     * Start the ConnectedThread which manages the bluetooth connection
     * @param socket: The BluetoothSocket on which the connection was made
     * @param device: the BlueTooth device which is connected
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device){
        Log.d(TAG,"connected");

        //cancel the thread which completed connection (good job thread! )
        if (mConnectThread != null) { mConnectThread.cancel(); mConnectThread = null;}

        // Cancel any thread trying to run connection
        if (mConnectedThread != null) { mConnectedThread.cancel(); mConnectedThread = null;}

        //Cancel the accept thread
        if (mAcceptThread != null) { mAcceptThread.cancel(); mAcceptThread = null; }

        //start thread to manage connection and transactions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        //send name of connected device back to UI
        Message msg = mHandler.obtainMessage(MainActivity.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);
    }

    /**
     * Stop all the threads
     */
    public synchronized void stop() {
        if (mConnectThread != null) { mConnectThread.cancel();mConnectThread = null; }
        if (mConnectedThread != null) {mConnectedThread.cancel();mConnectedThread =null; }
        if (mAcceptThread != null) { mAcceptThread.cancel();mAcceptThread = null; }
        setState(STATE_NONE);
    }

    /**
     * write to the ConnectedThread, unsyncronised manner
     * - out: the bytes to write
     */
    public void write(byte[] out) {
        ConnectedThread r;

        //synchronise a copy of connected thread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }

        //perform unsyncronised write
        r.write(out);
    }

    /**
     *  Notify UI activity (main) that connection was lost
     */
    private void connectionLost() {
        setState(STATE_NONE);

        //Send failure msg back to main activity
        Message msg = mHandler.obtainMessage(MainActivity.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.TOAST, "BallBot connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        //start the service over
        BTClient.this.start();
    }

    /**
     * Notify UI activity (main) that connection has failed
     */
    private void connectionFailed() {
        setState(STATE_NONE);

        //send failure msg back to main
        Message msg = mHandler.obtainMessage(MainActivity.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.TOAST, "Unable to connect to ball bot");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        //start the service over to restart in listening mode
        BTClient.this.start();
    }

    private class ConnectThread extends Thread {
        private BluetoothSocket mmSocket;
        private BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            //get bluetooth socket for connection with device
            try {
                tmp = mmDevice.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                Log.e("BTConnectDebug","Create() failed",e);
            }
            mmSocket = tmp;
        }

        public void run() {
            Log.i("BTConnectDebug","BEGIN mConnectThread");
            setName("ConnectThread");

            //cancelling discovery speeds up connection
            mBluetoothAdapter.cancelDiscovery();

            //connect to Bluetooth socket
            try {
                //blocking call, will only return on success or exception
                mmSocket.connect();
            } catch (IOException e) {
                try {
                    Log.e("BTConnectDebug","trying fallback...");

                    mmSocket =(BluetoothSocket) mmDevice.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(mmDevice,1);
                    mmSocket.connect();

                    Log.e("BTConnectDebug","Connected");
                }
                catch (Exception e2) {
                    //close socket
                    try{
                        mmSocket.close();
                    } catch (IOException e3) {
                        Log.e("BTConnectDebug","unable to close() socket during connection failure",e2);
                    }
                    Log.e("BTConnectDebug","mConnectThread failed e",e);
                    connectionFailed();
                    return;
                }
            }

            //reset connect thread
            synchronized (BTClient.this) {
                mConnectThread = null;
            }

            //start connected thread
            connected(mmSocket,mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG,"close of connect socket failed",e);
            }
        }
    }

    /**
     * Thread runs during connection
     * Handles incoming and outgoing data
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread (BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            //get socket input/output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG,"temp sockets not created",e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            Log.i(TAG,"BEGIN mConnectedThread");
            byte[] buffer = new byte[1024];
            int bytes;

            //keep listening to InputStream while connected
            while(true) {
                try {
                    //read bytes from input stream
                    bytes = mmInStream.read(buffer);
                    //send obtained bytes to main UI activity
                    mHandler.obtainMessage(MainActivity.MESSAGE_READ,bytes,-1,buffer)
                            .sendToTarget();

                } catch (IOException e){
                    Log.e(TAG,"DISCONNECTED",e);
                    connectionLost();
                    //start the service over in listening mode!
                    BTClient.this.start();
                    break;
                }
            }
        }

        /**
         * Write to the connected OutStream
         * @param buffer - byte array to write
         */
        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
                //send message back to main
                mHandler.obtainMessage(MainActivity.MESSAGE_WRITE,-1,-1,buffer).sendToTarget();
            }catch (IOException e){
                Log.e(TAG,"Exception during write",e);
            }
        }
        public void cancel() {
            try {
                mmSocket.close();
            }catch (IOException e) {
                Log.e(TAG,"close() of connect socket failed",e);
            }
        }
    }

    /**
     * This thread runs while listening for incoming connections.
     * Behaves like a server-side cleint.
     * Runs until a connection is accepted or cancelled
     */
    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread(){
            BluetoothServerSocket tmp = null;
            try {
                tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(CLIENT_NAME,MY_UUID);
            }catch (IOException e) {
                //
                Log.e(TAG,"listen() failed",e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            Log.d(TAG, "Begin mAcceptThread" + this);
            setName("AcceptThread");

            BluetoothSocket socket = null;

            //listen to server socket if not connected
            while (mState != STATE_CONNECTED) {
                try {
                    // This is a blocking call and will on return on a succesful cxn or excception
                    socket = mmServerSocket.accept();
                } catch (IOException e){
                    Log.e(TAG, "Accept() failed",e);
                    break;
                }

                //if connection was accepted
                if (socket != null) {
                    synchronized (BTClient.this){
                        switch (mState){
                            case STATE_LISTENING:
                            case STATE_CONNECTING:
                                //normal situation
                                connected(socket,socket.getRemoteDevice());
                                break;
                            case STATE_NONE:
                            case STATE_CONNECTED:
                                //either not ready or already connected
                                //terminate new socket
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    Log.e(TAG,"Could not close unwanted socket");
                                }
                                break;
                        }
                    }
                }
                Log.i(TAG,"END mAcceptThread");
            }
        }

        public void cancel() {
            Log.d(TAG, "Cancel" + this);
            try {
                mmServerSocket.close();
            } catch (IOException e){
                Log.e(TAG,"Close() of server failed",e);
            }
        }
    }

}
