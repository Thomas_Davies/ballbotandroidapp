package autodesk.com.ballbotandroidapp;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;


import android.bluetooth.BluetoothAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.MobileAnarchy.Android.Widgets.Joystick.JoystickMovedListener;
import com.MobileAnarchy.Android.Widgets.Joystick.JoystickView;

import java.lang.ref.WeakReference;


public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();

    // Message types sent from BTClient
    public static final int MESSAGE_STATE_CHANGED = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Layout view
    Button buttonTmp,buttonSnd,buttonHmd,buttonLht,buttonCxn;
    JoystickView mJoystick;
    TextView mStatusTxtView;
    TextView mTxtJoystickData;

    //Menu
    private MenuItem mItemConnect;

    //Timer Task
    private Handler mUpdateHandler;
    private int mTimeoutCounter = 0;
    private int mMaxTimeoutCount = 20;
    private long mUpdatePeriod = 200;

    //Joystick location
    private int mPan;
    private int mTilt;
    private boolean mJoystickCentered = true;

    //names recieved from BTClient
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    //Name of connected device
    private String mConnectedDeviceName = null;

    //local bluetooth adapter
    public BluetoothAdapter mBluetoothAdapter;

    //member object for bluetooth services
    private BTClient mBTClient = null;

    //message handler
    private final MyHandler mHandler = new MyHandler(this);

    /** Called when the activity is first created */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Remove title bar
        //this.requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //~~~~~~~~~~~~~~~ Layout View ~~~~~~~~~~~~~~~~~//
        //buttons
        buttonTmp = (Button) findViewById(R.id.buttonTmp);
        buttonHmd = (Button) findViewById(R.id.buttonHmd);
        buttonSnd = (Button) findViewById(R.id.buttonSnd);
        buttonLht = (Button) findViewById(R.id.buttonLht);
        mStatusTxtView = (TextView) findViewById(R.id.statusTextView);
        mTxtJoystickData = (TextView) findViewById(R.id.txtJoystickData);

        //joystick
        mJoystick = (JoystickView) findViewById(R.id.joystick);
        mJoystick.setOnJostickMovedListener(joystickListener);

        // Get local bluetooth adaptor
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, bluetooth is not supported
        if (mBluetoothAdapter == null)
        {
            Toast.makeText(this,"Bluetooth is not Available!",Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        //if bluetooth off, turn it on
        if (!mBluetoothAdapter.isEnabled()){
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBTIntent,1);
        }

        // Initialise bluetoothClient
        mBTClient = new BTClient(this, mHandler);

        mUpdateHandler = new Handler();
        mUpdateHandler.postDelayed(timerRunnable,mUpdatePeriod);
    }

    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            UpdateMethod();
            //tell handler to run again, effectively a clock!
            mUpdateHandler.postDelayed(this,mUpdatePeriod);
        }
    };

    private static class MyHandler extends Handler{
        //using weak reference won't prevent garbage collection
        private final WeakReference<MainActivity> mWeakReferenceMainActivity;

        public MyHandler(MainActivity mainActivity){
            mWeakReferenceMainActivity = new WeakReference<MainActivity>(mainActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity mainActivity = mWeakReferenceMainActivity.get();

            if (mainActivity != null){
                switch (msg.what){
                    case MESSAGE_STATE_CHANGED:
                        switch (msg.arg1){
                            case BTClient.STATE_CONNECTED:
                                mainActivity.mStatusTxtView.setText(R.string.status_connected_to);
                                mainActivity.mStatusTxtView.append(" " + mainActivity.mConnectedDeviceName);
                                break;
                            case BTClient.STATE_CONNECTING:
                                mainActivity.mStatusTxtView.setText(R.string.status_connecting);
                                break;
                            case BTClient.STATE_LISTENING:
                            case BTClient.STATE_NONE:
                                mainActivity.mStatusTxtView.setText(R.string.status_not_connected);
                                break;
                        }
                        break;
                    case MESSAGE_READ:
                        byte[] readBuf = (byte[])msg.obj;
                        //construct string from valid bytes.
                        String readMessage = new String(readBuf,0,msg.arg1);
                        Log.i(TAG,"RX: " + readMessage);
                        //PARSE THIS MESSAGE AND DISPLAY ON BUTTONS!

                        break;
                    case MESSAGE_DEVICE_NAME:
                        //save device name
                        mainActivity.mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                        Toast.makeText(mainActivity.getApplicationContext(), R.string.status_connected_to +
                                mainActivity.mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                        break;
                    case MESSAGE_TOAST:
                        Toast.makeText(mainActivity.getApplicationContext(),msg.getData().getString(TOAST),
                                Toast.LENGTH_SHORT).show();
                        break;
                }

            }
        }

    }



    private JoystickMovedListener joystickListener = new JoystickMovedListener() {
        @Override
        public void OnMoved(int pan, int tilt) {
            mPan = pan;
            mTilt = tilt;
            mTxtJoystickData.setText(String.format("( %d, %d )",mPan,mTilt));
            mJoystickCentered = false;
        }

        @Override
        public void OnReleased() {
        }

        @Override
        public void OnReturnedToCenter() {
            mPan = 0;
            mTilt = 0;
            UpdateMethod(); //update immediatly for instant stopping. (not triggering? why?!!!!)
                mJoystickCentered = true;
        }
    };

    private void UpdateMethod () {
        if (!mJoystickCentered || (mTimeoutCounter >= mMaxTimeoutCount)){
            //use STX & ETX, start and end string charecters improves reliability.
            sendMessage( new String(new byte[] { 0x02, (byte) (mPan+10), (byte) (mTilt+10), 0x03}));    //+10 to both since easier to process via python...
            mTimeoutCounter = 0;
        } else {
            mTimeoutCounter ++;
        }
    }

    /**
     * Sends message via BTClient
     * @param message - the string of text to be sent
     */
    private void sendMessage(String message){
        //check connection
        if (mBTClient.getState() != BTClient.STATE_CONNECTED){
            return;
        }

        //check theres data to send
        if (message.length() > 0) {
            //get message bytes and pass to client to send
            byte[] send = message.getBytes();
            mBTClient.write(send);
        }
    }

    public void onBluetoothSelect(View view){
        Intent serverIntent = new Intent(this,DeviceListActivity.class);
        startActivityForResult(serverIntent,1);
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if (mBTClient != null) {
            if (mBTClient.getState() == BTClient.STATE_NONE){
                mBTClient.start();
            }
        }
    }

    @Override
    public void onDestroy() {
        //stop timer
        mUpdateHandler.removeCallbacks(timerRunnable);
        //kill BTClient
        if (mBTClient != null) {
            mBTClient.stop();
        }
        super.onDestroy();
    }

    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case 1:
                //DeviceListActivity returns with device to connect
                if (resultCode == Activity.RESULT_OK){
                    //get mac address of device
                    String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    //get bluetooth device
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    //attempt connection to device
                    mBTClient.connect(device);
                }
                break;
            case 2:
                //when DeviceListActivity returns to enable bluetooth
                if(resultCode != Activity.RESULT_OK) {
                    //user didnt enable BT or error occured
                    Toast.makeText(this,"Bluetooth not enable or failed, leaving app",
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }
    }
}

